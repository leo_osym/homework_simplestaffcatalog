﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;

namespace Homework_StaffCatalog
{
    public partial class ShowForm : Form
    {
        List<Person> staff = new List<Person>();
        Person person;
        public ShowForm()
        {
            InitializeComponent();
            GetFromDb();
            foreach(var person in staff)
            {
                comboBox2.Items.Add($"[{person.ID}] {person.FirstName} {person.LastName}");
            }
            comboBox2.Refresh();


        }

        private void GetFromDb()
        {
            IDbConnection connection = new MySqlConnection(
                            new MySqlConnectionStringBuilder
                            {
                                Server = "127.0.0.1",
                                Database = "TestDatabase",
                                UserID = "sa",
                                Password = "root"
                            }.ConnectionString);
            try
            {
                connection.Open();

                IDbCommand command = connection.CreateCommand();
                
                command.CommandText = "SELECT * from mystaff";
                command.ExecuteNonQuery();
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    staff.Add(new Person { ID = reader.GetInt32(0),
                                            FirstName = reader.GetString(1),
                                            LastName = reader.GetString(2),
                                            Position = reader.GetString(3),
                                            Department = reader.GetString(4)});
                }

                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void comboBox2_DropDownClosed(object sender, EventArgs e)
        {
            int ID = Int32.Parse(comboBox2.SelectedItem.ToString().Split(new string[] { "[", "]" }, StringSplitOptions.RemoveEmptyEntries)[0]);
            foreach (var p in staff)
            {
                if (p.ID.Equals(ID)) person = p;
            }
            labelFirstName.Text = person.FirstName;
            labelLastName.Text = person.LastName;
            labelPosition.Text = person.Position;
            labelDepartment.Text = person.Department;
        }
    }
}
