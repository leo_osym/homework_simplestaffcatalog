﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;

namespace Homework_StaffCatalog
{
    public partial class AddPersonForm : Form
    {
        public AddPersonForm()
        {
            //buttonAdd.DialogResult = DialogResult.OK;
            //buttonCancel.DialogResult = DialogResult.Cancel;
            InitializeComponent();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(textBoxFirstName.Text))
                errorProvider.SetError(textBoxFirstName, "This field shouldn't be blank!");
            else if (String.IsNullOrWhiteSpace(textBoxLastName.Text))
                errorProvider.SetError(textBoxLastName, "This field shouldn't be blank!");
            else if (String.IsNullOrWhiteSpace(textBoxPosition.Text))
                errorProvider.SetError(textBoxPosition, "This field shouldn't be blank!");
            else if (String.IsNullOrWhiteSpace(textBoxDepartment.Text))
                errorProvider.SetError(textBoxDepartment, "This field shouldn't be blank!");
            else
            {
                IDbConnection connection = new MySqlConnection(
                            new MySqlConnectionStringBuilder
                            {
                                //Server = "192.168.0.101",
                                Server = "127.0.0.1",
                                Database = "TestDatabase",
                                UserID = "sa",
                                Password = "root"
                            }.ConnectionString);
                try
                {
                    connection.Open();

                    IDbCommand command = connection.CreateCommand();
                    command.CommandText =
                        //"DROP TABLE mystaff";
                        /*"CREATE TABLE mystaff(Id INT AUTO_INCREMENT PRIMARY KEY, " +
                        "FirstName VARCHAR(32) NOT NULL, " +
                        "LastName VARCHAR(32) NOT NULL," +
                        "Position VARCHAR(32) NOT NULL," +
                        "Department VARCHAR(32) NOT NULL)";*/
                    //command.ExecuteNonQuery();
                    command.CommandText = String.Format("INSERT INTO mystaff (FirstName,LastName,Position, Department) VALUES ('{0}','{1}','{2}','{3}')",
                        textBoxFirstName.Text.ToString(), textBoxLastName.Text.ToString(), textBoxPosition.Text.ToString(), textBoxDepartment.Text.ToString());
                    command.ExecuteNonQuery();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                Close();
            }
        }
    }
}
